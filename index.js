// console.log("hello world");

// Functions
/*
	- Functions in JS are lines/block of codes that tell device/appn to perform a certain task when called/invoked

	- Functions are mostly created to create complicated tasks to run several lines of codes 


*/

// Function Declarations
	//function statement defines a function with specified parameters

	/*
		Syntax:
			function functionName() {
				code block(statement);

			}

		function keyword - used to define JS functions
		functionName - function name. Functions are named to be able to use later in code
		function block ({}) - statements which comprise body of function. This is where code is to be executed
	*/ 

	function printName(){
		console.log("My name is John!");
	}

//Function Invocation 
	/*
		The code block and statements inside a function is not immediately executed when function is defined. The code block and statements inside a function is executed only when function is invoked or called
	*/

	printName();

	// declaredFunction();//cannot invoke function that has yet to be defined

// Function Declarations vs Function Expression
	// Function Declarations

	declaredFunction(); //declared functions can be hoisted. As long as function has been declared

	function declaredFunction(){
		console.log("Hello, from the declaredFunction()");
	}

//Function Expression
 	//Function can be stored in variable. Called a function expression

 	//Function expression is an anonymous function assigned to variableFunction

 	//Anonymous function - function without name 

 	// variableFunction(); //cannot access 'variableFunction' before initialization

 	let variableFunction = function(){
 		console.log("Hello Again!");
 	}

 	variableFunction();

 	let funcExpression = function funcName() {
 		console.log("Hello from the other side!");
 	}

 	funcExpression();
 	// funcName();//not defined b/c already assigned to a variable

 // Can reassign declared function and function expression to a new anonymous function

 	declaredFunction = function() {
 		console.log("Updated declaredFunction");
 	}

 	declaredFunction();

 	funcExpression = function(){
 		console.log("Updated funcExpression")
 	}

 	funcExpression();

	 const constantFunc = function() {
	 	console.log("Initialized with const");
	 }

	 constantFunc();

	 // constantFunc = function(){
	 // 	console.log("Can be reassigned?")
	 // }

	 // constantFunc(); 

 // Function Scoping 
/*
	1. local/block scope
	2. global scope
	3. function scope
*/

	{
		let localVar = "Juan Dela Cruz";
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	// console.log(localVar); 
	
	//function scope 
	function showNames(){
		var functionVar = "Mark";
		const functionConst = "Jayson";
		let functionLet = "Arnel";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);
	// these can only be accessed inside function

//Nested Functions 
	
	function myNewFunction(){
		let name = "Gabriella";
		
		function nestedFunction(){
			let nestedName = "Maria"; 
			console.log(name);
		}

		nestedFunction();

		// console.log(nestedName); // results in an error
		// nestedName variable, being declared in the nested function cannot be accessed outside of function it was declared in 
	} 

	myNewFunction();

	//nestedFunction(); //results in an error

// Function and Global Scoped variables
	// Global scoped variable
	let globalName = "Alexandro";

	function myNewFunction2() {
		let nameInside = "Khabib";

		console.log(globalName);
	}

	myNewFunction2();

	// console.log(nameInside); //result in an error

// Using Alert()

// alert() allows us to show small window at top of browser page to show info to user

alert("Hello, JavaScript");

function showSampleAlert(){
	alert("Hello, User!");
}

showSampleAlert();

console.log("I will only log in the console when alert is dismissed");

// Using Prompt 
	// prompt() allows us to show small window at top of browser to gather user input. Much like alert(), will also have page wait until user completes or enters input. Input from prompt() will be returned as string once user dismisses the window 
/*
	Syntax:
		prompt("<dialogInString>");
	
*/ 
let samplePrompt = prompt("Enter your name");

console.log("Hello, "+ samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");

	console.log ("Hello, "+ firstName + " " + lastName);
	alert("Welcome to my page!");
}

printWelcomeMessage();

// Function Naming Conventions
	
	function getCourses() {

		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();

	// Avoid generic names to avoid confusion within code

	function get(){

		let name = "Jamie";
		console.log(name);
	}

	get();

	// Avoid pointless and inappropriate function names 

	function foo (){
		console.log(25%5);
	}

	foo(); 

	// Name your functions in small caps. Follow camel case when naming variables and functions

	function displayCarInfo(){

		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500.00");
	}

	displayCarInfo();